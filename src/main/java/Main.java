import config.AppConfig;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        Logger log = Logger.getLogger(Main.class.getName());
        ApiContextInitializer.init();
        log.info("Program is started " + LocalDateTime.now());
        TelegramBotsApi botApi = new TelegramBotsApi();

        Properties prop = new AppConfig("/home/najanaja/Development/Source/java/TGBot/src/main/resources/config.propeties").GetConfig();

        try{
            botApi.registerBot(new JessieBot(prop));
            log.info("Bot is Registered " + LocalDateTime.now());
        }
        catch (TelegramApiException ex){
            log.warning(ex.getMessage());
        }
    }
}

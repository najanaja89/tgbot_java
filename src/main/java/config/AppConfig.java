package config;

import java.io.FileReader;
import java.util.List;
import java.util.Properties;

public class AppConfig {
    private  static String path1;
    public AppConfig(String path) {
        path1 = path;
    }

    public static Properties GetConfig(){
        Properties properties = new Properties();
        try {
            FileReader fileReader = new FileReader(path1);

            properties.load(fileReader);
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return properties;
    }
}

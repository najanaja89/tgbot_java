import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

public class JessieBot extends TelegramLongPollingBot{
    private Properties prop;

    static Logger log = Logger.getLogger(JessieBot.class.getName());

    public JessieBot(Properties prop) {
        this.prop = prop;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if(update.hasMessage() && update.getMessage().hasText()){
            SendMessage sendMessage = new SendMessage();
            String message = update.getMessage().getText();
            String resMes;
            long chatId = update.getMessage().getChatId();
            log.info("GOT MESSAGE ");
            if(message.equals("/start")) {
                resMes = "Вы все раки";
                ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
                List<KeyboardRow> row = new ArrayList<>();
                KeyboardRow keyboardButtons = new KeyboardRow();
                keyboardButtons.add("/menu");
                keyboardButtons.add("/order");
                keyboardButtons.add("/info");
                keyboardButtons.add("/address");

                row.add(keyboardButtons);// row

                /*KeyboardRow keyboardButtons2 = new KeyboardRow();
                keyboardButtons2.add("Stroka 2 Button 1");
                keyboardButtons2.add("Stroka 2 Button 2");
                keyboardButtons2.add("Stroka 2 Button 3");
                keyboardButtons2.add("Stroka 2 Button 4");
                row.add(keyboardButtons);// row*/

                replyKeyboardMarkup.setKeyboard(row);
                sendMessage.setReplyMarkup(replyKeyboardMarkup);
                sendMessage.setText("Choose command");
            }


            else {
                resMes = "please enter command";
            }

            sendMessage.setChatId(chatId);
            sendMessage.setText(resMes);
            try{
                execute(sendMessage);
            }catch (TelegramApiException ex ){
                log.warning(ex.getMessage());
            }
        }
    }

    @Override
    public String getBotUsername() {
        return prop.getProperty("botname");
    }

    @Override
    public String getBotToken() {
        return prop.getProperty("bottoken");
    }
}
